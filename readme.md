# README
Readmeを書いてみよう

# 見出しの大きさ
## 2
\#が2つ
### 3
\#が3つ
###### 6
\#が6つ

# 装飾
### イタリック体
toya yuto  
*toya yuto*  
_toya yuto_

### 太字
遠矢 侑音  
**遠矢 侑音**  
__遠矢　侑音__

### 取り消し線
~~取り消し線を消してください~~

# リンク
ここで[moodle日大](https://moodle.ce.cst.nihon-u.ac.jp/)に飛べます。

# テーブル
|日付|時間帯|食べたもの|
|--------|--------|-------|
|2/10|昼|すき家|
|2/10|夜|マクドナルド|